<?php
require 'DeIntercompany.php';
class Intercompany_model extends CI_Model
{
	var $numPed = 0;
	var $codProveedor = 0;
	var $rutProveedor = 0;
	var $codSociedad = 0;
	var $materiales = array();

    public function __construct()
    {
    	parent::__construct();
    }

    public function insertOrder()
    {
        $dbConn;
        //seleccción de bd
        if($this->codProveedor == 2000 || $this->codProveedor == 2100)
        {
            $dbConn = "dmcCLTEST";
        }
        elseif ($this->codProveedor == 2200) {
            $dbConn = "dmcPRTEST";
        }
        else
        {
            $dbConn = "uniTEST";
        }
        $this->load->database($dbConn);

        //comprobación duplicados
        $textSql = "SELECT COUNT(*) PEDIDOEXISTE FROM EN_INTERCOMPANY
                    WHERE SAP_PED_INTER = ?
                    AND SAP_SOC_EMISOR = ?";
        $query = $this->db->query($textSql,array($this->numPed ,$this->codProveedor));

        $pedidoExiste = $query->result()[0]->PEDIDOEXISTE;

        if($pedidoExiste == 0) //si el pedido no existe
        {
            //inicio transacción
            $this->db->trans_start();
            //particularidades dimerc
            if($this->codSociedad != 2200) //grabo cosprom para todas las sociedades salvo Dimerc Perú
            {
                /*if($this->codSociedad == 2000)
                {
                    //rol aplicación para grabar, sólo Dimerc Chile
                    $textSql = "SET ROLE ROL_APLICACION";
                    $this->db->query($textSql);
                }*/
                $sqlDetalle = "insert into de_intercompany(SAP_SOC_EMISOR, SAP_PED_INTER, SAP_ITM_PROD, SAP_COD_PROD, SAP_CAN_VTA, SAP_UND_VTA, SAP_PRE_VTA, SAP_MON_VTA,SAP_COSPROM)
                        values(?,?,?,?,?,?,?,?,?)";
            }
            else
            {
                $sqlDetalle = "insert into de_intercompany(SAP_SOC_EMISOR, SAP_PED_INTER, SAP_ITM_PROD, SAP_COD_PROD, SAP_CAN_VTA, SAP_UND_VTA, SAP_PRE_VTA, SAP_MON_VTA)
                        values(?,?,?,?,?,?,?,?)";
            }

            
            foreach ($this->materiales as $material)
            {
                if($this->codSociedad != 2200) //grabo cosprom para todas las sociedades salvo Dimerc Perú
                {
                    $textSql = "SELECT NVL(MAX(A.cosprom),0) COSPROM
                                from ma_costosxcentros a, ma_product b
                                where a.codpro = b.codpro
                                and b.codsap = ?";
                    $query = $this->db->query($textSql,array($material->codMaterial));
                    $cosprom = $query->result()[0]->COSPROM;
                    $arrInsert = array($this->codProveedor, $this->numPed,
                                                         $material->itmProd, $material->codMaterial,
                                                         $material->cantidad, $material->unidCantidad,
                                                         $material->precioNeto, $material->moneda,$cosprom);
                }
                else
                {
                    $arrInsert = array($this->codProveedor, $this->numPed,
                                                         $material->itmProd, $material->codMaterial,
                                                         $material->cantidad, $material->unidCantidad,
                                                         $material->precioNeto, $material->moneda);
                }
                $query = $this->db->query($sqlDetalle, $arrInsert);
            }

            $textSql = "SELECT NVL(CODEMP,0) CODEMP FROM MA_EMPRESA
                        WHERE SAP_SOCIEDAD = ?";
            $query = $this->db->query($textSql,array($this->codSociedad));
            $codemp = $query->result()[0]->CODEMP;

            $sqlCabecera = "insert into en_intercompany(SAP_PED_INTER,SAP_SOC_EMISOR,SAP_RUT_PROVE,SAP_SOC_DESTINO,LEG_CODEMP)
                        values(?,?,?,?,?)";
            $query = $this->db->query($sqlCabecera,array($this->numPed, $this->codProveedor, $this->rutProveedor, $this->codSociedad, $codemp));

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else
            {
                $this->db->trans_commit();
            }
            $this->db->close();
        }
        else
        {
            $this->db->close();
        }
    }

    public function addMaterial($itmProd, $codMaterial, $cantidad, $unidCantidad, $precioNeto, $moneda)
    {
    	$material = new DeIntercompany();
    	$material->itmProd = $itmProd;
    	$material->codMaterial = $codMaterial;
    	$material->cantidad = $cantidad;
    	$material->unidCantidad = $unidCantidad;
    	$material->precioNeto = $precioNeto;
    	$material->moneda = $moneda;

    	array_push($this->materiales, $material);
    }
}