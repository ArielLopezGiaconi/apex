<?php
	function guardaLogBruto($logBruto, $estado)
	{
		include(APPPATH.'config/database.php');
		$user = $db['uniTEST']['username'];
		$password = $db['uniTEST']['password'];
		$stringDB = $db['uniTEST']['hostname'];

		$connOra = oci_connect($user,$password,$stringDB) or die (ocierror());
		$sql = "insert into HOLDING.LOG_XML_SAP(id,xml_recibido,fecha_recepcion,estado)
                    values(HOLDING.xml_error_sap.NEXTVAL, empty_clob(),sysdate, :estado)
                    RETURNING xml_recibido INTO :xmlentrada";

	            $stid = oci_parse($connOra, $sql);
	            $myLob = oci_new_descriptor($connOra, OCI_D_LOB);
	            oci_bind_by_name($stid, ":xmlentrada", $myLob, -1, OCI_B_CLOB);
	            oci_bind_by_name($stid, ":estado", $estado, 5);
	            oci_execute($stid, OCI_NO_AUTO_COMMIT); // use OCI_DEFAULT for PHP <= 5.3.1
	            $myLob->save($logBruto);

	            oci_commit($connOra);
	}
 ?>